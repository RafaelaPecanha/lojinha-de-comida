<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projeto2' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gGpQDHTF)j!gL1<~_>F -QFeAb#*W6a6?m)3Ibl8fp87[^Q*CX<cy-tF~Ye9tS($' );
define( 'SECURE_AUTH_KEY',  'u&L|0C$#|3VOxfD&yuJ!ULmD|J=]mW4c_rSR;.p)Vw3[$%@?gG0KZ$(zc3O5*u2.' );
define( 'LOGGED_IN_KEY',    'L?,^Ig %HJKx1/9F0e4Q?RvB^x5b:-M<gFroY&zk3E*NJYX,t:UH7/Eo6AG@,xCT' );
define( 'NONCE_KEY',        '-CX<cE0cB2WVQr}wXCTb.zN6]2Jl$@Y)|(ve9yK<t[BvC(hO`KF]z@m7Gn:O^F-~' );
define( 'AUTH_SALT',        'j2Fg=j-_7!Gt1<?<y->^7mWaHU]>YD|/Q4XV*zqW(;56n;.6QzC@#F~G8*zN?HTj' );
define( 'SECURE_AUTH_SALT', '?3t^RKJSOi@T~dJOL=&a~[S4PF-0`0kd#]N:kg?I+Jd?#2)/ItRVTzb*Uw6`l*bb' );
define( 'LOGGED_IN_SALT',   ',7SHMiYL_bU*ZUlG.<SAa*$/c9<~CnU#(Iy}%2gi$|zSq/G8q!jgFsKNW<uwu1/{' );
define( 'NONCE_SALT',       '<iW$xA7y`tz fb=^G.3V74D6/BWvkAO}(?`)XwCc,_IpEwSe,wz0y%B$1xvYM >P' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
