<?php
    function wootreino_add_woocomerce_support() {
       add_theme_support('woocommerce');
       
    }
    add_theme_support('menus');
    add_action('after_setup_therme', 'wootreino_add_woocomerce_suport');

    function wootreino_css() {
        wp_register_style('wootreino-style', get_template_directory_uri(). '/style.css');
        wp_enqueue_style('wootreino-style');
    }

    add_action('wp_enqueue_scripts', 'wootreino_css');

  
    function get_link_category_img(){
        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;

        $args = array(
               'taxonomy'     => $taxonomy,
               'orderby'      => $orderby,
               'show_count'   => $show_count,
               'pad_counts'   => $pad_counts,
               'hierarchical' => $hierarchical,
               'title_li'     => $title,
               'hide_empty'   => $empty
        );
        $categorias_lista = [];
        $categories = get_categories($args);

        
        if ($categories){
            
           foreach($categories as $category){
            
            if($category->name !='Sem categoria'){
                      
                        $category_id = $category->term_id;
                        $img_id = get_term_meta($category_id, 'thumbnail_id', true);
                        $categorias_lista[] = [
                            'name' => $category->name,
                            'id'=> $category_id,
                            'link'=> get_term_link($category_id, 'product_cat'),
                            'img'=> wp_get_attachment_image_src($img_id, 'slide')[0]
                        ];
                };  
               
            };
        };
        return $categorias_lista;
    };


    function dia_da_semana(){
        return date_i18n('l');
    }

    function format_products($products, $img_size='medium'){
        $product_final =[];
        foreach ($products as $product){
            $product_final[] = [
                'name' => $product->get_name(),
                'price' => $product->get_price_html(),
                'link'=> $product->get_permalink(),
                'img'=> wp_get_attachment_image_src($product->get_image_id(),$img_size)[0],
            ];
        }
        return $product_final;
    }


   function product_list($products){ ?>
    
        <ul class = 'products_list'>
            <?php foreach($products as $product){?>
                    <li class = 'product-item'>
                        <a href ="<?= $product['link'];?>">
                            <div class = 'product-info'>
                                <img src = "<?=$product['img'];?>"alt ="<?= $product['name'];?>">
                                <h1><?= $product['name'];?> 
                                <h1><?= $product['price'];?></h1>
                            </div>
                            <div class = 'product-overlay'>
                               <img src="<?= get_stylesheet_directory_uri() . '/imagens';?>/carrinho_ver_mais.png" alt ="">
                            </div>
                        </a>
                    </li>
            <?php } ?>
        </ul>
    <?php       
   }

   function busca_filtro($categoria){
    $args = array(
        'category' => array($categoria),
    );
    $products = wc_get_products($args );
   }
   

?>