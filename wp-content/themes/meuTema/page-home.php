    <?php 
    //Template name: home
    get_header();?>
    
    <section class = "painel1"> 
        <h1 class = "titulo">Comes&Bebes</h1>
        <p class = "texto1">O restaurante para todas as fomes</p>
    </section>
    <section class = "painel2">
        <p class = "">CONHEÇA NOSSA LOJA</p>  
        <p>Tipo de pratos principais</p>
    </section>   
 

<div class ='categorias'>
<?php
  
    $categoria_final = get_link_category_img();
    foreach($categoria_final as $category){
        if($category['name'] !='Sem categoria'){
            echo '<a href = "'.$category['link'].'" class = ""><img src="'.$category['img'].'" alt =""></a>';
            echo '<h1> '.$category['name'].'  </h1>';
        }
        
    }
?>
</div>

<p>Pratos do dia de hoje:</p>
<?php echo date_i18n('l');?>

<div class = 'do_dia'>
<?php

$product_week = [];
$data = [];

$product_week = wc_get_products([
    'limit'=> 4,
    'tag'=>[dia_da_semana()],
]);



$data['product_week'] = format_products($product_week );

?>
<main>
    <?php if($data['product_week']){?>
        <?php product_list($data['product_week'])?>
        <?php } ?>
</main>

</div>

<a href = "<?= get_stylesheet_directory_uri() . '/loja';?>/" class = "loja">
    <button>Veja outras opções</button>
</a>

   
<?php get_footer(); ?>

