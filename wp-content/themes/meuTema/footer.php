<?php wp_footer();?>

<footer class = 'rodape'>
    <p>© Copyright 2022 - Desenvolvido por Rafaela Peçanha</p>

    <div id = "modal-promocao" class = 'modal-container'>
        <div class = 'modal'>
            <button class = 'fechar' >x</button>
            <h3 class = 'subtitulo'>carrinho</h3>
            
            <form>
                <input type='text' class = 'input' placeholder='email'>
                <input type='button' class = 'button' placeholder='cadastrar'>
            </form>
        </div>

    </div>
    <script>

        function iniciaModal(modalID) {
            const modal = document.getElementById(modalID);
            if(modal) {
                modal.classList.add('mostrar');
                modal.addEventListener('click', (evento) => {
                    if(evento.target.id == modalID || evento.target.className =='fechar'){
                        modal.classList.remove('mostrar');
                    }
                });
            }
        }
        const logo = document.querySelector('.carrinho');
        logo.addEventListener('click',() => iniciaModal('modal-promocao'));

    </script>

</footer>

</body>
</html>